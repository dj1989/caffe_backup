var searchData=
[
  ['has_5fignore_5flabel_5f',['has_ignore_label_',['../classcaffe_1_1AccuracyLayer.html#a4acdfaf6db79fbe1983b8439391ad15e',1,'caffe::AccuracyLayer::has_ignore_label_()'],['../classcaffe_1_1SoftmaxWithLossLayer.html#ad77bc32fa576ad025102d29acf79aefb',1,'caffe::SoftmaxWithLossLayer::has_ignore_label_()']]],
  ['hdf5datalayer',['HDF5DataLayer',['../classcaffe_1_1HDF5DataLayer.html',1,'caffe']]],
  ['hdf5outputlayer',['HDF5OutputLayer',['../classcaffe_1_1HDF5OutputLayer.html',1,'caffe']]],
  ['height',['height',['../classcaffe_1_1Blob.html#a422a10a605c30ac02a5377e7cf4c8c6c',1,'caffe::Blob']]],
  ['hingelosslayer',['HingeLossLayer',['../classcaffe_1_1HingeLossLayer.html',1,'caffe']]]
];
