var searchData=
[
  ['sgdsolver',['SGDSolver',['../classcaffe_1_1SGDSolver.html',1,'caffe']]],
  ['sigmoidcrossentropylosslayer',['SigmoidCrossEntropyLossLayer',['../classcaffe_1_1SigmoidCrossEntropyLossLayer.html',1,'caffe']]],
  ['sigmoidlayer',['SigmoidLayer',['../classcaffe_1_1SigmoidLayer.html',1,'caffe']]],
  ['silencelayer',['SilenceLayer',['../classcaffe_1_1SilenceLayer.html',1,'caffe']]],
  ['slicelayer',['SliceLayer',['../classcaffe_1_1SliceLayer.html',1,'caffe']]],
  ['softmaxlayer',['SoftmaxLayer',['../classcaffe_1_1SoftmaxLayer.html',1,'caffe']]],
  ['softmaxwithlosslayer',['SoftmaxWithLossLayer',['../classcaffe_1_1SoftmaxWithLossLayer.html',1,'caffe']]],
  ['solver',['Solver',['../classcaffe_1_1Solver.html',1,'caffe']]],
  ['splitlayer',['SplitLayer',['../classcaffe_1_1SplitLayer.html',1,'caffe']]],
  ['spplayer',['SPPLayer',['../classcaffe_1_1SPPLayer.html',1,'caffe']]],
  ['syncedmemory',['SyncedMemory',['../classcaffe_1_1SyncedMemory.html',1,'caffe']]]
];
