var searchData=
[
  ['inferblobshape',['InferBlobShape',['../classcaffe_1_1DataTransformer.html#a1e43b0fb80cded5bb854e1a06004bebf',1,'caffe::DataTransformer::InferBlobShape(const Datum &amp;datum)'],['../classcaffe_1_1DataTransformer.html#ac829dd6448dcce67acb1e22a2554db4b',1,'caffe::DataTransformer::InferBlobShape(const vector&lt; Datum &gt; &amp;datum_vector)'],['../classcaffe_1_1DataTransformer.html#abb299b1fdc4a859597a6eafc30147eee',1,'caffe::DataTransformer::InferBlobShape(const vector&lt; cv::Mat &gt; &amp;mat_vector)'],['../classcaffe_1_1DataTransformer.html#a34c4e5baeac4b442f7336ac3410fb7b5',1,'caffe::DataTransformer::InferBlobShape(const cv::Mat &amp;cv_img)']]],
  ['init',['Init',['../classcaffe_1_1Net.html#ae9fcfaabc89165d6c0cb4b14b4c6b584',1,'caffe::Net']]],
  ['initrand',['InitRand',['../classcaffe_1_1DataTransformer.html#a6d807c7dc250e66b62d97d9847278e68',1,'caffe::DataTransformer']]],
  ['inputdebuginfo',['InputDebugInfo',['../classcaffe_1_1Net.html#ad9ca7b6ee2615e493f62dc23d340818d',1,'caffe::Net']]]
];
