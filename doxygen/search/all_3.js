var searchData=
[
  ['datalayer',['DataLayer',['../classcaffe_1_1DataLayer.html',1,'caffe']]],
  ['datatransformer',['DataTransformer',['../classcaffe_1_1DataTransformer.html',1,'caffe']]],
  ['db',['DB',['../classcaffe_1_1db_1_1DB.html',1,'caffe::db']]],
  ['debug_5finfo_5f',['debug_info_',['../classcaffe_1_1Net.html#addc4e5501070ede24155d4c1bf9928a2',1,'caffe::Net']]],
  ['deconvolutionlayer',['DeconvolutionLayer',['../classcaffe_1_1DeconvolutionLayer.html',1,'caffe']]],
  ['diff_5fscale_5f',['diff_scale_',['../classcaffe_1_1PowerLayer.html#aa83169eaa1b573137aa6ed2b526879f0',1,'caffe::PowerLayer']]],
  ['dim_5f',['dim_',['../classcaffe_1_1ReductionLayer.html#a5d0cdd8ca00e6bbd425a51bef2214a7e',1,'caffe::ReductionLayer']]],
  ['dropoutlayer',['DropoutLayer',['../classcaffe_1_1DropoutLayer.html#a24cbddd4699b102a9555d3b8013c16d0',1,'caffe::DropoutLayer']]],
  ['dropoutlayer',['DropoutLayer',['../classcaffe_1_1DropoutLayer.html',1,'caffe']]],
  ['dummydatalayer',['DummyDataLayer',['../classcaffe_1_1DummyDataLayer.html',1,'caffe']]]
];
